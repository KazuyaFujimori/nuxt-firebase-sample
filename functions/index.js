const functions = require('firebase-functions')
const express = require('express')
const { Nuxt } = require('nuxt')

const app = express()

const config = {
  dev: false,
  buildDir: 'nuxt',
  build: {
    publicPath: '/'
  }
}
const nuxt = new Nuxt(config)

function handleRequest(path, req, res) {
  res.set('Cache-Control', 'public, max-age=600, s-maxage=1200')
  nuxt.renderRoute(path, { req }).then(result => {
    return res.send(result.html)
  }).catch(e => {
    console.error(e)
    return res.send(e)
  })
}

function handleRoot(req, res) {
  handleRequest('/', req, res)
}

function handleOther(req, res) {
  handleRequest('/other', req, res)
}

app.get('/other', handleOther)
app.get('*', handleRoot)
exports.ssrapp = functions.https.onRequest(app)
